<?php

$stmt_func = [
    'Expr_Eval' => 'eval',
    'Stmt_Eval' => 'eval',
    'Stmt_Echo' => 'echo',
    'Expr_Print' => 'print',
    'Expr_Exit' => 'exit',
    'Expr_Include' => 'include',
    'Expr_Isset' => 'isset'
];   //语句型函数



class Sources
{
    public static $V_USERINPUT = array(
        '_GET',
        '_POST',
        '_COOKIE',
        '_REQUEST',
        '_FILES',
        '_SERVER',
        'HTTP_GET_VARS',
        'HTTP_POST_VARS',
        'HTTP_COOKIE_VARS',
        'HTTP_REQUEST_VARS',
        'HTTP_POST_FILES',
        'HTTP_SERVER_VARS',
        'HTTP_RAW_POST_DATA',
        'argc',
        'argv'
    );

    public static $V_SERVER_PARAMS = array(
        'HTTP_ACCEPT',
        'HTTP_ACCEPT_LANGUAGE',
        'HTTP_ACCEPT_ENCODING',
        'HTTP_ACCEPT_CHARSET',
        'HTTP_CONNECTION',
        'HTTP_HOST',
        'HTTP_KEEP_ALIVE',
        'HTTP_REFERER',
        'HTTP_USER_AGENT',
        'HTTP_X_FORWARDED_FOR',
        'PHP_AUTH_DIGEST',
        'PHP_AUTH_USER',
        'PHP_AUTH_PW',
        'AUTH_TYPE',
        'QUERY_STRING',
        'REQUEST_METHOD',
        'REQUEST_URI', // partly urlencoded
        'PATH_INFO',
        'ORIG_PATH_INFO',
        'PATH_TRANSLATED',
        'REMOTE_HOSTNAME',
        'PHP_SELF'
    );

    public static $F_FILE_INPUT = array(
        'bzread',
        'dio_read',
        'exif_imagetype',
        'exif_read_data',
        'exif_thumbnail',
        'fgets',
        'fgetss',
        'file',
        'file_get_contents',
        'fread',
        'get_meta_tags',
        'glob',
        'gzread',
        'readdir',
        'read_exif_data',
        'scandir',
        'zip_read'
    );

    public static $F_DATABASE_INPUT = array(
        'mysql_fetch_array',
        'mysql_fetch_assoc',
        'mysql_fetch_field',
        'mysql_fetch_object',
        'mysql_fetch_row',
        'pg_fetch_all',
        'pg_fetch_array',
        'pg_fetch_assoc',
        'pg_fetch_object',
        'pg_fetch_result',
        'pg_fetch_row',
        'sqlite_fetch_all',
        'sqlite_fetch_array',
        'sqlite_fetch_object',
        'sqlite_fetch_single',
        'sqlite_fetch_string'
    );

    public static $F_OTHER_INPUT = array(
        'get_headers',
        'getallheaders',
        'get_browser',
        'getenv',
        'gethostbyaddr',
        'runkit_superglobals',
        'import_request_variables'
    );

}



$F_SECURING_BOOL = array(
    'is_bool',
    'is_double',
    'is_float',
    'is_real',
    'is_long',
    'is_int',
    'is_integer',
    'is_null',
    'is_numeric',
    'is_finite',
    'is_infinite',
    'ctype_alnum',
    'ctype_alpha',
    'ctype_cntrl',
    'ctype_digit',
    'ctype_xdigit',
    'ctype_upper',
    'ctype_lower',
    'ctype_space',
    'in_array',
    'preg_match',
    'preg_match_all',
    'fnmatch',
    'ereg',
    'eregi'
);

$F_SECURING_STRING = array(
    'intval',
    'floatval',
    'doubleval',
    'filter_input',
    'urlencode',
    'rawurlencode',
    'round',
    'floor',
    'strlen',
    'strrpos',
    'strpos',
    'strftime',
    'strtotime',
    'md5',
    'md5_file',
    'sha1',
    'sha1_file',
    'crypt',
    'crc32',
    'hash',
    'mhash',
    'hash_hmac',
    'password_hash',
    'mcrypt_encrypt',
    'mcrypt_generic',
    'base64_encode',
    'ord',
    'sizeof',
    'count',
    'bin2hex',
    'levenshtein',
    'abs',
    'bindec',
    'decbin',
    'dechex',
    'decoct',
    'hexdec',
    'rand',
    'max',
    'min',
    'metaphone',
    'tempnam',
    'soundex',
    'money_format',
    'number_format',
    'date_format',
    'filetype',
    'nl_langinfo',
    'bzcompress',
    'convert_uuencode',
    'gzdeflate',
    'gzencode',
    'gzcompress',
    'http_build_query',
    'lzf_compress',
    'zlib_encode',
    'imap_binary',
    'iconv_mime_encode',
    'bson_encode',
    'sqlite_udf_encode_binary',
    'session_name',
    'readlink',
    'getservbyport',
    'getprotobynumber',
    'gethostname',
    'gethostbynamel',
    'gethostbyname',
);

$F_INSECURING_STRING = array(
    'base64_decode',
    'htmlspecialchars_decode',
    'html_entity_decode',
    'bzdecompress',
    'chr',
    'convert_uudecode',
    'gzdecode',
    'gzinflate',
    'gzuncompress',
    'lzf_decompress',
    'rawurldecode',
    'urldecode',
    'zlib_decode',
    'imap_base64',
    'imap_utf7_decode',
    'imap_mime_header_decode',
    'iconv_mime_decode',
    'iconv_mime_decode_headers',
    'hex2bin',
    'quoted_printable_decode',
    'imap_qprint',
    'mb_decode_mimeheader',
    'bson_decode',
    'sqlite_udf_decode_binary',
    'utf8_decode',
    'recode_string',
    'recode'
);

$F_SECURING_XSS = array(
    'htmlentities',
    'htmlspecialchars',
    'highlight_string',
);

$F_SECURING_SQL = array(
    'addslashes',
    'dbx_escape_string',
    'db2_escape_string',
    'ingres_escape_string',
    'maxdb_escape_string',
    'maxdb_real_escape_string',
    'mysql_escape_string',
    'mysql_real_escape_string',
    'mysqli_escape_string',
    'mysqli_real_escape_string',
    'pg_escape_string',
    'pg_escape_bytea',
    'sqlite_escape_string',
    'sqlite_udf_encode_binary',
    'cubrid_real_escape_string',
);

$F_SECURING_PREG = array(
    'preg_quote'
);

$F_SECURING_FILE = array(
    'basename',
    'dirname',
    'pathinfo'
);

$F_SECURING_SYSTEM = array(
    'escapeshellarg',
    'escapeshellcmd'
);

$F_SECURING_XPATH = array(
    'addslashes'
);

$F_SECURING_LDAP = array(
);

$F_SECURES_ALL = array_merge(
    $F_SECURING_XSS,
    $F_SECURING_SQL,
    $F_SECURING_PREG,
    $F_SECURING_FILE,
    $F_SECURING_SYSTEM,
    $F_SECURING_XPATH
);

$F_QUOTE_ANALYSIS = $F_SECURING_SQL;



$NAME_XSS = 'Cross-Site Scripting';
$F_XSS = array(
    'echo'							=> array(array(0), $F_SECURING_XSS),
    'print'							=> array(array(1), $F_SECURING_XSS),
    'print_r'						=> array(array(1), $F_SECURING_XSS),
    'exit'							=> array(array(1), $F_SECURING_XSS),
    'die'							=> array(array(1), $F_SECURING_XSS),
    'printf'						=> array(array(0), $F_SECURING_XSS),
    'vprintf'						=> array(array(0), $F_SECURING_XSS),
    'trigger_error'					=> array(array(1), $F_SECURING_XSS),
    'user_error'					=> array(array(1), $F_SECURING_XSS),
    'odbc_result_all'				=> array(array(2), $F_SECURING_XSS),
    'ovrimos_result_all'			=> array(array(2), $F_SECURING_XSS),
    'ifx_htmltbl_result'			=> array(array(2), $F_SECURING_XSS)
);

$NAME_HTTP_HEADER = 'HTTP Response Splitting';
$F_HTTP_HEADER = array(
    'header' 						=> array(array(1), array())
);

$NAME_SESSION_FIXATION = 'Session Fixation';
$F_SESSION_FIXATION = array(
    'setcookie' 					=> array(array(2), array()),
    'setrawcookie' 					=> array(array(2), array()),
    'session_id' 					=> array(array(1), array())
);

$NAME_CODE = 'Code Execution';
$F_CODE = array(
    'assert' 						=> array(array(1), array()),
    'create_function' 				=> array(array(1,2), array()),
    'eval' 							=> array(array(1), array()),
    'mb_ereg_replace'				=> array(array(1,2), $F_SECURING_PREG),
    'mb_eregi_replace'				=> array(array(1,2), $F_SECURING_PREG),
    'preg_filter'					=> array(array(1,2), $F_SECURING_PREG),
    'preg_replace'					=> array(array(1,2), $F_SECURING_PREG),
    'preg_replace_callback'			=> array(array(1), $F_SECURING_PREG),
);

$NAME_REFLECTION = 'Reflection Injection';
$F_REFLECTION = array(
    'event_buffer_new'				=> array(array(2,3,4), array()),
    'event_set'						=> array(array(4), array()),
    'iterator_apply'				=> array(array(2), array()),
    'forward_static_call'			=> array(array(1), array()),
    'forward_static_call_array'		=> array(array(1), array()),
    'call_user_func'				=> array(array(1), array()),
    'call_user_func_array'			=> array(array(1), array()),
    'array_diff_uassoc'				=> array(array(3), array()),
    'array_diff_ukey'				=> array(array(3), array()),
    'array_filter'					=> array(array(2), array()),
    'array_intersect_uassoc'		=> array(array(3), array()),
    'array_intersect_ukey'			=> array(array(3), array()),
    'array_map'						=> array(array(1), array()),
    'array_reduce'					=> array(array(2), array()),
    'array_udiff'					=> array(array(3), array()),
    'array_udiff_assoc'				=> array(array(3), array()),
    'array_udiff_uassoc'			=> array(array(3,4), array()),
    'array_uintersect'				=> array(array(3), array()),
    'array_uintersect_assoc'		=> array(array(3), array()),
    'array_uintersect_uassoc'		=> array(array(3,4), array()),
    'array_walk'					=> array(array(2), array()),
    'array_walk_recursive'			=> array(array(2), array()),
    'assert_options'				=> array(array(2), array()),
    'ob_start'						=> array(array(1), array()),
    'register_shutdown_function'	=> array(array(1), array()),
    'register_tick_function'		=> array(array(1), array()),
    'runkit_method_add'				=> array(array(1,2,3,4), array()),
    'runkit_method_copy'			=> array(array(1,2,3), array()),
    'runkit_method_redefine'		=> array(array(1,2,3,4), array()),
    'runkit_method_rename'			=> array(array(1,2,3), array()),
    'runkit_function_add'			=> array(array(1,2,3), array()),
    'runkit_function_copy'			=> array(array(1,2), array()),
    'runkit_function_redefine'		=> array(array(1,2,3), array()),
    'runkit_function_rename'		=> array(array(1,2), array()),
    'session_set_save_handler'		=> array(array(1,2,3,4,5), array()),
    'set_error_handler'				=> array(array(1), array()),
    'set_exception_handler'			=> array(array(1), array()),
    'spl_autoload'					=> array(array(1), array()),
    'spl_autoload_register'			=> array(array(1), array()),
    'sqlite_create_aggregate'		=> array(array(2,3,4), array()),
    'sqlite_create_function'		=> array(array(2,3), array()),
    'stream_wrapper_register'		=> array(array(2), array()),
    'uasort'						=> array(array(2), array()),
    'uksort'						=> array(array(2), array()),
    'usort'							=> array(array(2), array()),
    'yaml_parse'					=> array(array(4), array()),
    'yaml_parse_file'				=> array(array(4), array()),
    'yaml_parse_url'				=> array(array(4), array()),
    'eio_busy'						=> array(array(3), array()),
    'eio_chmod'						=> array(array(4), array()),
    'eio_chown'						=> array(array(5), array()),
    'eio_close'						=> array(array(3), array()),
    'eio_custom'					=> array(array(1,2), array()),
    'eio_dup2'						=> array(array(4), array()),
    'eio_fallocate'					=> array(array(6), array()),
    'eio_fchmod'					=> array(array(4), array()),
    'eio_fchown'					=> array(array(5), array()),
    'eio_fdatasync'					=> array(array(3), array()),
    'eio_fstat'						=> array(array(3), array()),
    'eio_fstatvfs'					=> array(array(3), array()),
    'preg_replace_callback'			=> array(array(2), array()),
    'dotnet_load'					=> array(array(1), array()),
);

$NAME_FILE_INCLUDE = 'File Inclusion';
$F_FILE_INCLUDE = array(
    'include' 						=> array(array(1), $F_SECURING_FILE),
    'include_once' 					=> array(array(1), $F_SECURING_FILE),
    'parsekit_compile_file'			=> array(array(1), $F_SECURING_FILE),
    'php_check_syntax' 				=> array(array(1), $F_SECURING_FILE),
    'require' 						=> array(array(1), $F_SECURING_FILE),
    'require_once' 					=> array(array(1), $F_SECURING_FILE),
    'runkit_import'					=> array(array(1), $F_SECURING_FILE),
    'set_include_path' 				=> array(array(1), $F_SECURING_FILE),
    'virtual' 						=> array(array(1), $F_SECURING_FILE)
);

$NAME_FILE_READ = 'File Disclosure';
$F_FILE_READ = array(
    'bzread'						=> array(array(1), $F_SECURING_FILE),
    'bzflush'						=> array(array(1), $F_SECURING_FILE),
    'dio_read'						=> array(array(1), $F_SECURING_FILE),
    'eio_readdir'					=> array(array(1), $F_SECURING_FILE),
    'fdf_open'						=> array(array(1), $F_SECURING_FILE),
    'file'							=> array(array(1), $F_SECURING_FILE),
    'file_get_contents'				=> array(array(1), $F_SECURING_FILE),
    'finfo_file'					=> array(array(1,2), array()),
    'fflush'						=> array(array(1), $F_SECURING_FILE),
    'fgetc'							=> array(array(1), $F_SECURING_FILE),
    'fgetcsv'						=> array(array(1), $F_SECURING_FILE),
    'fgets'							=> array(array(1), $F_SECURING_FILE),
    'fgetss'						=> array(array(1), $F_SECURING_FILE),
    'fread'							=> array(array(1), $F_SECURING_FILE),
    'fpassthru'						=> array(array(1,2), array()),
    'fscanf'						=> array(array(1), $F_SECURING_FILE),
    'ftok'							=> array(array(1), $F_SECURING_FILE),
    'get_meta_tags'					=> array(array(1), $F_SECURING_FILE),
    'glob'							=> array(array(1), array()),
    'gzfile'						=> array(array(1), $F_SECURING_FILE),
    'gzgetc'						=> array(array(1), $F_SECURING_FILE),
    'gzgets'						=> array(array(1), $F_SECURING_FILE),
    'gzgetss'						=> array(array(1), $F_SECURING_FILE),
    'gzread'						=> array(array(1), $F_SECURING_FILE),
    'gzpassthru'					=> array(array(1), $F_SECURING_FILE),
    'highlight_file'				=> array(array(1), $F_SECURING_FILE),
    'imagecreatefrompng'			=> array(array(1), $F_SECURING_FILE),
    'imagecreatefromjpg'			=> array(array(1), $F_SECURING_FILE),
    'imagecreatefromgif'			=> array(array(1), $F_SECURING_FILE),
    'imagecreatefromgd2'			=> array(array(1), $F_SECURING_FILE),
    'imagecreatefromgd2part'		=> array(array(1), $F_SECURING_FILE),
    'imagecreatefromgd'				=> array(array(1), $F_SECURING_FILE),
    'opendir'						=> array(array(1), $F_SECURING_FILE),
    'parse_ini_file' 				=> array(array(1), $F_SECURING_FILE),
    'php_strip_whitespace'			=> array(array(1), $F_SECURING_FILE),
    'readfile'						=> array(array(1), $F_SECURING_FILE),
    'readgzfile'					=> array(array(1), $F_SECURING_FILE),
    'readlink'						=> array(array(1), $F_SECURING_FILE),
    'scandir'						=> array(array(1), $F_SECURING_FILE),
    'show_source'					=> array(array(1), $F_SECURING_FILE),
    'simplexml_load_file'			=> array(array(1), $F_SECURING_FILE),
    'stream_get_contents'			=> array(array(1), $F_SECURING_FILE),
    'stream_get_line'				=> array(array(1), $F_SECURING_FILE),
    'xdiff_file_bdiff'				=> array(array(1,2), $F_SECURING_FILE),
    'xdiff_file_bpatch'				=> array(array(1,2), $F_SECURING_FILE),
    'xdiff_file_diff_binary'		=> array(array(1,2), $F_SECURING_FILE),
    'xdiff_file_diff'				=> array(array(1,2), $F_SECURING_FILE),
    'xdiff_file_merge3'				=> array(array(1,2,3), $F_SECURING_FILE),
    'xdiff_file_patch_binary'		=> array(array(1,2), $F_SECURING_FILE),
    'xdiff_file_patch'				=> array(array(1,2), $F_SECURING_FILE),
    'xdiff_file_rabdiff'			=> array(array(1,2), $F_SECURING_FILE),
    'yaml_parse_file'				=> array(array(1), $F_SECURING_FILE),
    'zip_open'						=> array(array(1), $F_SECURING_FILE)
);

$NAME_FILE_AFFECT = 'File Manipulation';
$F_FILE_AFFECT = array(
    'bzwrite'						=> array(array(2), array()),
    'chmod'							=> array(array(1), $F_SECURING_FILE),
    'chgrp'							=> array(array(1), $F_SECURING_FILE),
    'chown'							=> array(array(1), $F_SECURING_FILE),
    'copy'							=> array(array(1), array()),
    'dio_write'						=> array(array(1,2), array()),
    'eio_chmod'						=> array(array(1), $F_SECURING_FILE),
    'eio_chown'						=> array(array(1), $F_SECURING_FILE),
    'eio_mkdir'						=> array(array(1), $F_SECURING_FILE),
    'eio_mknod'						=> array(array(1), $F_SECURING_FILE),
    'eio_rmdir'						=> array(array(1), $F_SECURING_FILE),
    'eio_write'						=> array(array(1,2), array()),
    'eio_unlink'					=> array(array(1), $F_SECURING_FILE),
    'error_log'						=> array(array(3), $F_SECURING_FILE),
    'event_buffer_write'			=> array(array(2), array()),
    'file_put_contents'				=> array(array(1,2), $F_SECURING_FILE),
    'fputcsv'						=> array(array(1,2), $F_SECURING_FILE),
    'fputs'							=> array(array(1,2), $F_SECURING_FILE),
    'fprintf'						=> array(array(0), array()),
    'ftruncate'						=> array(array(1), $F_SECURING_FILE),
    'fwrite'						=> array(array(1,2), $F_SECURING_FILE),
    'gzwrite'						=> array(array(1,2), array()),
    'gzputs'						=> array(array(1,2), array()),
    'loadXML'						=> array(array(1), array()),
    'mkdir'							=> array(array(1), array()),
    'move_uploaded_file'			=> array(array(1,2), $F_SECURING_FILE),
    'posix_mknod'					=> array(array(1), $F_SECURING_FILE),
    'recode_file'					=> array(array(2,3), $F_SECURING_FILE),
    'rename'						=> array(array(1,2), $F_SECURING_FILE),
    'rmdir'							=> array(array(1), $F_SECURING_FILE),
    'shmop_write'					=> array(array(2), array()),
    'touch'							=> array(array(1), $F_SECURING_FILE),
    'unlink'						=> array(array(1), $F_SECURING_FILE),
    'vfprintf'						=> array(array(0), array()),
    'xdiff_file_bdiff'				=> array(array(3), $F_SECURING_FILE),
    'xdiff_file_bpatch'				=> array(array(3), $F_SECURING_FILE),
    'xdiff_file_diff_binary'		=> array(array(3), $F_SECURING_FILE),
    'xdiff_file_diff'				=> array(array(3), $F_SECURING_FILE),
    'xdiff_file_merge3'				=> array(array(4), $F_SECURING_FILE),
    'xdiff_file_patch_binary'		=> array(array(3), $F_SECURING_FILE),
    'xdiff_file_patch'				=> array(array(3), $F_SECURING_FILE),
    'xdiff_file_rabdiff'			=> array(array(3), $F_SECURING_FILE),
    'yaml_emit_file'				=> array(array(1,2), $F_SECURING_FILE),
);

$NAME_EXEC = 'Command Execution';
$F_EXEC = array(
    'backticks'						=> array(array(1), $F_SECURING_SYSTEM), # transformed during parsing
    'exec'							=> array(array(1), $F_SECURING_SYSTEM),
    'expect_popen'					=> array(array(1), $F_SECURING_SYSTEM),
    'passthru'						=> array(array(1), $F_SECURING_SYSTEM),
    'pcntl_exec'					=> array(array(1), $F_SECURING_SYSTEM),
    'popen'							=> array(array(1), $F_SECURING_SYSTEM),
    'proc_open'						=> array(array(1), $F_SECURING_SYSTEM),
    'shell_exec'					=> array(array(1), $F_SECURING_SYSTEM),
    'system'						=> array(array(1), $F_SECURING_SYSTEM),
    'mail'							=> array(array(5), array()), // http://esec-pentest.sogeti.com/web/using-mail-remote-code-execution
    'mb_send_mail'					=> array(array(5), array()),
    'w32api_invoke_function'		=> array(array(1), array()),
    'w32api_register_function'		=> array(array(2), array()),
);

$NAME_DATABASE = 'SQL Injection';
$F_DATABASE = array(
    'dba_open'						=> array(array(1), array()),
    'dba_popen'						=> array(array(1), array()),
    'dba_insert'					=> array(array(1,2), array()),
    'dba_fetch'						=> array(array(1), array()),
    'dba_delete'					=> array(array(1), array()),
    'dbx_query'						=> array(array(2), $F_SECURING_SQL),
    'odbc_do'						=> array(array(2), $F_SECURING_SQL),
    'odbc_exec'						=> array(array(2), $F_SECURING_SQL),
    'odbc_execute'					=> array(array(2), $F_SECURING_SQL),
    'db2_exec' 						=> array(array(2), $F_SECURING_SQL),
    'db2_execute'					=> array(array(2), $F_SECURING_SQL),
    'fbsql_db_query'				=> array(array(2), $F_SECURING_SQL),
    'fbsql_query'					=> array(array(1), $F_SECURING_SQL),
    'ibase_query'					=> array(array(2), $F_SECURING_SQL),
    'ibase_execute'					=> array(array(1), $F_SECURING_SQL),
    'ifx_query'						=> array(array(1), $F_SECURING_SQL),
    'ifx_do'						=> array(array(1), $F_SECURING_SQL),
    'ingres_query'					=> array(array(2), $F_SECURING_SQL),
    'ingres_execute'				=> array(array(2), $F_SECURING_SQL),
    'ingres_unbuffered_query'		=> array(array(2), $F_SECURING_SQL),
    'msql_db_query'					=> array(array(2), $F_SECURING_SQL),
    'msql_query'					=> array(array(1), $F_SECURING_SQL),
    'msql'							=> array(array(2), $F_SECURING_SQL),
    'mssql_query'					=> array(array(1), $F_SECURING_SQL),
    'mssql_execute'					=> array(array(1), $F_SECURING_SQL),
    'mysql_db_query'				=> array(array(2), $F_SECURING_SQL),
    'mysql_query'					=> array(array(1), $F_SECURING_SQL),
    'mysql_unbuffered_query'		=> array(array(1), $F_SECURING_SQL),
    'mysqli_stmt_execute'			=> array(array(1), $F_SECURING_SQL),
    'mysqli_query'					=> array(array(2), $F_SECURING_SQL),
    'mysqli_real_query'				=> array(array(1), $F_SECURING_SQL),
    'mysqli_master_query'			=> array(array(2), $F_SECURING_SQL),
    'oci_execute'					=> array(array(1), array()),
    'ociexecute'					=> array(array(1), array()),
    'ovrimos_exec'					=> array(array(2), $F_SECURING_SQL),
    'ovrimos_execute'				=> array(array(2), $F_SECURING_SQL),
    'ora_do'						=> array(array(2), array()),
    'ora_exec'						=> array(array(1), array()),
    'pg_query'						=> array(array(2), $F_SECURING_SQL),
    'pg_send_query'					=> array(array(2), $F_SECURING_SQL),
    'pg_send_query_params'			=> array(array(2), $F_SECURING_SQL),
    'pg_send_prepare'				=> array(array(3), $F_SECURING_SQL),
    'pg_prepare'					=> array(array(3), $F_SECURING_SQL),
    'sqlite_open'					=> array(array(1), $F_SECURING_SQL),
    'sqlite_popen'					=> array(array(1), $F_SECURING_SQL),
    'sqlite_array_query'			=> array(array(1,2), $F_SECURING_SQL),
    'arrayQuery'					=> array(array(1,2), $F_SECURING_SQL),
    'singleQuery'					=> array(array(1), $F_SECURING_SQL),
    'sqlite_query'					=> array(array(1,2), $F_SECURING_SQL),
    'sqlite_exec'					=> array(array(1,2), $F_SECURING_SQL),
    'sqlite_single_query'			=> array(array(2), $F_SECURING_SQL),
    'sqlite_unbuffered_query'		=> array(array(1,2), $F_SECURING_SQL),
    'sybase_query'					=> array(array(1), $F_SECURING_SQL),
    'sybase_unbuffered_query'		=> array(array(1), $F_SECURING_SQL)
);

$NAME_XPATH = 'XPath Injection';
$F_XPATH = array(
    'xpath_eval'					=> array(array(2), $F_SECURING_XPATH),
    'xpath_eval_expression'			=> array(array(2), $F_SECURING_XPATH),
    'xptr_eval'						=> array(array(2), $F_SECURING_XPATH)
);

$NAME_LDAP = 'LDAP Injection';
$F_LDAP = array(
    'ldap_add'						=> array(array(2,3), $F_SECURING_LDAP),
    'ldap_delete'					=> array(array(2), $F_SECURING_LDAP),
    'ldap_list'						=> array(array(3), $F_SECURING_LDAP),
    'ldap_read'						=> array(array(3), $F_SECURING_LDAP),
    'ldap_search'					=> array(array(3), $F_SECURING_LDAP)
);

$NAME_CONNECT = 'Protocol Injection';
$F_CONNECT = array(
    'curl_setopt'					=> array(array(2,3), array()),
    'curl_setopt_array' 			=> array(array(2), array()),
    'cyrus_query' 					=> array(array(2), array()),
    'error_log'						=> array(array(3), array()),
    'fsockopen'						=> array(array(1), array()),
    'ftp_chmod' 					=> array(array(2,3), array()),
    'ftp_exec'						=> array(array(2), array()),
    'ftp_delete' 					=> array(array(2), array()),
    'ftp_fget' 						=> array(array(3), array()),
    'ftp_get'						=> array(array(2,3), array()),
    'ftp_nlist' 					=> array(array(2), array()),
    'ftp_nb_fget' 					=> array(array(3), array()),
    'ftp_nb_get' 					=> array(array(2,3), array()),
    'ftp_nb_put'					=> array(array(2), array()),
    'ftp_put'						=> array(array(2,3), array()),
    'get_headers'					=> array(array(1), array()),
    'imap_open'						=> array(array(1), array()),
    'imap_mail'						=> array(array(1), array()),
    'mail' 							=> array(array(1,4), array()),
    'mb_send_mail'					=> array(array(1,4), array()),
    'ldap_connect'					=> array(array(1), array()),
    'msession_connect'				=> array(array(1), array()),
    'pfsockopen'					=> array(array(1), array()),
    'session_register'				=> array(array(0), array()),
    'socket_bind'					=> array(array(2), array()),
    'socket_connect'				=> array(array(2), array()),
    'socket_send'					=> array(array(2), array()),
    'socket_write'					=> array(array(2), array()),
    'stream_socket_client'			=> array(array(1), array()),
    'stream_socket_server'			=> array(array(1), array()),
    'printer_open'					=> array(array(1), array())
);

$NAME_OTHER = 'Possible Flow Control'; // :X
$F_OTHER = array(
    'dl' 							=> array(array(1), array()),
    'ereg'							=> array(array(2), array()), # nullbyte injection affected
    'eregi'							=> array(array(2), array()), # nullbyte injection affected
    'ini_set' 						=> array(array(1,2), array()),
    'ini_restore'					=> array(array(1), array()),
    'runkit_constant_redefine'		=> array(array(1,2), array()),
    'runkit_method_rename'			=> array(array(1,2,3), array()),
    'sleep'							=> array(array(1), array()),
    'usleep'						=> array(array(1), array()),
    'extract'						=> array(array(1), array()),
    'mb_parse_str'					=> array(array(1), array()),
    'parse_str'						=> array(array(1), array()),
    'putenv'						=> array(array(1), array()),
    'set_include_path'				=> array(array(1), array()),
    'apache_setenv'					=> array(array(1,2), array()),
    'define'						=> array(array(1), array()),
    'is_a'							=> array(array(1), array()) // calls __autoload()
);

$NAME_POP = 'PHP Object Injection';
$F_POP = array(
    'unserialize'					=> array(array(1), array()), // calls gadgets
    'yaml_parse'					=> array(array(1), array())	 // calls unserialize
);

$dangrous_fun = array_merge($F_XSS, $F_POP, $F_OTHER, $F_CONNECT, $F_LDAP, $F_XPATH, $F_DATABASE,$F_EXEC,$F_FILE_AFFECT,$F_FILE_READ,$F_FILE_INCLUDE,$F_REFLECTION,$F_CODE,$F_SESSION_FIXATION,$F_HTTP_HEADER);

?>