# pop_master

#### 介绍
2021年强网杯pop_master赛题exp

#### 软件架构
采用php-parser作为编译前端，然后基于此做污点分析


#### 安装教程

1.  php版本大于7.4
2.  安装php-parser
(后续如果有时间的话，我搭建个docker)

#### 使用说明

1.  将赛题中class.php的内容复制到本exp的code.php中
2.  然后将mian.php中的全局变量中的入口方法与入口参数名赋值
3.  运行main.php
