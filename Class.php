<?php

class func_call_info{
    public $stmt_num;
    public $params;
}

class func_path_info{
    public $function_names;
    public $infos;
}

class Func_Call_Map{
    public $function_name; //函数名
    public $call_function; //该调用函数的函数，是一个二元数组类型，key为函数名，value为一个数组，数组中全是数字，每一个数字都代表了这个函数是在第几个语句被调用的，之所以用数组是因为怕有多个语句调用了同一个函数。
    public $called_function; //调用该函数的函数，具体类型同上
    public $tag;  //标记该函数是否为敏感函数，如果tag为0表示不敏感，tag为1表示返回值与函数的参数有关，tag为2表示返回值与source点有关，tag为4表示与全局变量有关，其表示方式为二进制，与Linux文件权限表示方式相同。
    public $params;
}

class relation{
    public $from;
    public $to;
    public $condition;
}

class CFG_class{
    public $blocks;  //存放代码块，二维数组
    public $map;   //存放各个块之间的联系
}