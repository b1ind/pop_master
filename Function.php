<?php
$maked_function = [];   //存放函数的定义是否被处理过，key保存函数名，value为0表示安全函数，value为1表示危险函数


function array_fix($array1 , $array2){
    foreach ($array1 as $value){
        foreach ($array2 as $value2){
            if($value === $value2)
                return 1;
        }
    }
    return 0;
}



function BinaryOp_Concat($ob){
    $flag = 0;
    if(get_class($ob->left) === 'PhpParser\Node\Expr\Variable'){
        if(in_array($ob->left->name,Sources::$V_USERINPUT)){
            $flag = 1;
        }
    }

    if (get_class($ob->right) === 'PhpParser\Node\Scalar\String_'){

        if(in_array($ob->right->var->name,Sources::$V_USERINPUT)){
            $flag = 1;
        }

    }
}



function deal_assign($ob,$vars){
    global $maked_function,$no_dirty_var,$no_dirty_vars;

    $var = $ob->var;
    $expr = $ob->expr;
    $flag = 0; //标记脏数据

    if(get_class($var) === "PhpParser\Node\Expr\Variable"){
        $var = $var -> name;
    }elseif (get_class($var) === "PhpParser\Node\Expr\ArrayDimFetch"){
        if(get_class($var->dim->value) ===  "PhpParser\Node\Scalar\String_"){
            $var = $var->var->name [$var->dim->value];
        }
    }//elseif...

    if(get_class($expr) === "PhpParser\Node\Expr\Variable"){
        $expr = $expr -> name;
    }elseif (get_class($expr) === "PhpParser\Node\Expr\ArrayDimFetch"){
        if(get_class($expr->dim) ===  "PhpParser\Node\Scalar\String_"){
            if(in_array($expr->var->name,Sources::$V_USERINPUT)){
                $flag = 1;
            }

        }
    }elseif (get_class($expr) === "PhpParser\Node\Expr\BinaryOp\Concat"){
        $flag = BinaryOp_Concat($expr);
    }elseif (get_class($expr) === "PhpParser\Node\Expr\FuncCall"){
        if($maked_function[$expr->name->parts[0]] === 1) {
            foreach ($maked_function as $key=>$value){
                foreach($expr->args as $value2){
                    if($key === $value2->value->name){
                        $flag = 1;
                    }
                }
            }
        }
    }//elseif...


    if($flag === 1 && ($vars === NULL || !in_array($var, $vars))){

        $vars[] = $var;
        if(in_array($var, $no_dirty_var)){
            $no_dirty_var = array_flip($no_dirty_var);
            unset($no_dirty_var[$var]);
            $no_dirty_var = array_flip($no_dirty_var);
        }
    }


    if($flag === 0 && in_array($var, $vars)){
        $no_dirty_var[] = $var;

    }
    return $vars;
}


